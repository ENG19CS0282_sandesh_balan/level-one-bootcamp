//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>
float input();
float find_volume(float x,float y,float z);
void output(float x,float y,float z,float volume);
int main()
{
    float h,d,b,v;
    h=input();
    d=input();
    b=input();
    v=find_volume(h,d,b);
    output(h,d,b,v);
    return 0;
}

float input()
{
    int x;
    printf("Enter a number: ");
    scanf("%d",&x);
    return x;
}

float find_volume(float x, float y, float z)
{
    float volume;
    volume=0.333333*((x*y*z)+(y/z));
    return volume;
}

void output(float x, float y, float z,float volume)
{
    printf("\nvolume of trombloid is: %f\n",volume);
}