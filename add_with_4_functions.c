//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>
int input();
int find_sum(int x,int y);
void output(int x, int y, int sum);
int main()
{
    int a,b,c;
    a=input();
    b=input();
    c=find_sum(a,b);
    output(a,b,c);
    return 0;
}

int input()
{
    int x;
    printf("Enter a number: ");
    scanf("%d",&x);
    return x;
}

int find_sum(int x,int y)
{
    int sum;
    sum=x+y;
    return sum;
}

void output(int x, int y, int sum)
{
    printf("\nsum of %d and %d is: %d\n",x,y,sum);
}