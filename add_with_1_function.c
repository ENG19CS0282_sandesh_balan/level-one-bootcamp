//Write a program to add two user input numbers using one function.

#include<stdio.h>
int main()
{
    int a,b,c;
    printf("Enter number1: ");
    scanf("%d",&a);
    printf("\nEnter number2: ");
    scanf("%d",&b);
    c=a+b;
    printf("\nSum of %d and %d is: %d\n",a,b,c);
    return 0;
}